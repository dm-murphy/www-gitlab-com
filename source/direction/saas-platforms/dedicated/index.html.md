---
layout: markdown_page
title: "Category Direction - GitLab Dedicated"
description: "GitLab Dedicated is GitLab's single-tenant, Software as a Service (SaaS) offering"
canonical_path: "/direction/saas-platforms/dedicated/"
---

- TOC
{:toc}

This page outlines the Direction for the GitLab Dedicated category and belongs to the [GitLab Dedicated](https://about.gitlab.com/handbook/product/categories/#gitlab-dedicated-group) group. To provide feedback or ask questions about this product category, reach out to the [Product Manager](mailto:athomas@gitlab.com).

This document and linked pages contain information related to upcoming products, features, and functionality. It is important to note that the information presented is for informational purposes only. Please do not rely on this information for purchasing or planning purposes. As with all projects, the items mentioned in this document and linked pages are subject to change or delay. The development, release, and timing of any products, features, or functionality remain at the sole discretion of GitLab Inc.

<!--more-->

You can contribute to this category by:

- Commenting on a relevant epic or issue in [our project](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team)(GitLab internal), or opening a new issue in the [GitLab Dedicated issue tracker](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/new?issue%5Bmilestone_id%5D=).
- Joining the discussion in the [#f_gitlab_dedicated](https://gitlab.slack.com/archives/C01S0QNSYJ2) Slack channel

## Overview
<!-- A good description of what your category is today or in the near term. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Provide enough context that someone unfamiliar
with the details of the category can understand what is being discussed. -->

GitLab Dedicated's objective is to provide a GitLab SaaS offering to large enterprises and customers in industries with strict security and compliance requirements. When adopting DevOps tools, customers [increasingly prefer SaaS](https://about.gitlab.com/direction/enablement/dotcom/#market-opportunity) to reduce operational cost, but customers in highly-regulated industries (e.g. Healthcare, Finance, and Public sectors) can't compromise on their security and compliance requirements when moving to SaaS. These requirements (e.g. isolated storage of source code IP) often dictate the need to be on separate Cloud Infrastructure from other tenants. This makes it challenging for these customers to adopt a shared-tenancy SaaS solution like gitlab.com.

Additionally, customers in regulated industries often require the ability to connect users or services running in their corporate network to their source code repositories running in the cloud via a private network connection. As a public-facing SaaS service, GitLab.com can't support this type of private connectivity easily by default. Finally, large, globally-distributed enterprises need the ability to control the location and number of Geo Replica sites to reduce latency for their workforce around the world. Some of our Self-Managed customers have 5+ Geo sites, and for them to consume SaaS they may need more global coverage than [what we offer on GitLab.com today](https://about.gitlab.com/direction/enablement/dotcom/#overview).

For these customers, a higher level of tenant isolation and deployment customization is required. While some customers may be satisfied leveraging a [partner MSP](#channel-partner-role-in-gitlab-dedicated), customers may prefer to consume the service directly from GitLab so that they have a single-point of contact for operational accountability.

GitLab Dedicated will solve these needs by offering a fully isolated, private GitLab instance, deployed in the customer's cloud regions of choice. The instance is fully hosted and managed by GitLab Inc enabling customers to offload operational overhead and focus on more business-critical tasks.

In addition to delivering customer value, GitLab Dedicated will also benefit internal GitLab teams. We envision the tooling we build for Dedicated being used as the single method going forward for running all of GitLab Inc's SaaS services. This will allow us to achieve even greater returns on our initial investment by enabling any future SaaS services to quickly get started with a robust and standardized foundation.

## Why is this important?
<!--
- Why is GitLab building this feature?
- What impact will it have on the broader devops workflow?
- How confident are we? What is the effort?
-->

There are two major trends emerging when it comes to the delivery of Cloud based solutions:

- Move to SaaS [as detailed in the .com strategy](https://about.gitlab.com/direction/enablement/dotcom/#market-opportunity) (IDC predicts that the market for SaaS-based DevOps tools (currently 38% of the market) will overtake the self-managed market (62%) by 2022 and account for $10.3B in revenue (or 58% of DevOps software tools market) by 2024),
- Adoption of Private Cloud, which makes it easier for enterprises to meet their security and compliance needs. Increasing fragmentation of global policy [regarding data protection](https://insights.comforte.com/13-countries-with-gdpr-like-data-privacy-laws) drives new compliance requirements, which can be better met with Private Cloud deployments. 16% of cloud services are expected to be delivered as a hosted private cloud offering [by 2025](https://www.reportlinker.com/p05826094/Global-Private-Cloud-Server-Market.html?utm_source=PRN).

GitLab Dedicated is for organizations that want to benefit from both SaaS and Private Cloud. By delivering this offering we will set ourselves up to achieve customer success as these two trends further accelerate in 2025.

### Target Audience
<!--
List the personas (https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas#user-personas) involved in this category.

Look for differences in user's goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->

#### Target Customer

GitLab Dedicated is initially targeted at large enterprises and customers in highly-regulated industries. 
- These customers want a SaaS solution but prefer not to use multi-tenant systems like GitLab.com due to strict security and compliance requirements or the desire to be on separate infrastructure to meet custom deployment needs. 
- Additionally, our target segment is comfortable with a 2000 seat minimum order (during [Limited Availability](#limited-availability)— otherwise GitLab Dedicated is cost prohibitive) and is willing to pay an [additional fee](#pricing) on top of licenses for a GitLab instance to be hosted and managed on their behalf. 
- Finally, our target segment has the flexibility to work with the [launch timelines](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/blob/main/README.md#timeline)(GitLab internal) we've established.

#### Positioning

GitLab Dedicated is for customers that need isolation from other tenants at the infrastructure level or need custom deployments of GitLab [as discussed above](#overview). 

The main value proposition with GitLab Dedicated is that we're giving customers their own GitLab Instance, isolated from other tenants, and hosted as a SaaS service on the customer's behalf. With this extra value comes an additional management fee. Customers who want SaaS, but are OK with the shared-tenant model of .com or don't want to pay an extra fee should use gitlab.com. Availability will not be a point of differentiation between GitLab Dedicated and .com as both offerings target a 99.95% Availability SLA. Meanwhile, customers who want full control over their GitLab installation should use the self-managed option.

**See below for a comparison of the different ways to deploy GitLab:**

![alt text](./Deployment_Options_GitLab_Dedicated.png "GitLab Deployment Options")

#### Target User Persona

- [Sidney (Systems Administrator)](https://about.gitlab.com/handbook/product/personas/#sidney-systems-administrator)

#### Target Buyer

- [Kennedy (Infrastructure Engineering Director)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#kennedy---the-infrastructure-engineering-director)

### Challenges to address
<!--
- What needs, goals, or jobs to be done do the users have?
- How do users address these challenges today? What products or work-arounds are utilized?

Provide links to UX Research issues, which validate these problems exist.
-->

Overall, the main challenge our target customer has is maintaining an up-to-date GitLab instance that fulfills their security and compliance requirements. More granular customer needs are documented in the sections below.

Note, this section does not reflect the list of currently available features within the GitLab Dedicated offering; rather, it contains the needs we're hearing from our customer base. The list of currently supported features in GitLab Dedicated is documented here: [https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/#available-features](https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/#available-features).

#### Migration

- Users want to seamlessly migrate their data, which can exceed many TBs in size, to GitLab Dedicated in a timely and reliable fashion from their existing DevOps solution, whether it's a third-party offering like BitBucket, a self-managed GitLab instance, or gitlab.com.

#### Security and Compliance

- Our target customer base, large enterprises and customers in highly-regulated industries, expect the offering to implement security best practices including encryption of data at rest and in transit, periodic encryption key rotation, ability to specify their own encryption key, integration with their own identity management systems, protection against DDoS attacks, intrusion detection, role-based access control for operators, segregation of duties with least privilege access configured among operator accounts, and approved escalation in order to access customer environments during break-glass scenarios.
- Upgrades: Customers want regular, zero-downtime upgrades to their instance along with timely security patches for high-sev issues when necessary.
- Data residency: Customers want their Dedicated instance and data (including logs and backups) to be hosted in their cloud region of choice to help meet their security and compliance requirements.
- Private Networking: Customers want to be able to securely connect applications running in their VPC including their Runner pool to GitLab Dedicated via a private connection without the traffic leaving the Cloud Provider backbone.
- Public Networking: Some customers work with third-parties (e.g. contractors), who do not authenticate through their main network and thus need to access the GitLab Dedicated instance over the public internet. In this scenario, customers need the ability to restrict access to their instance based on an allowlist of trusted IP ranges.

#### Observability

- Customer admins want to access telemetry data including health metrics and application/system logs so that they can monitor their instance and keep their internal teams informed regarding an outage or performance degradations with their DevOps environment. Users also want access to audit logs to achieve compliance requirements and the ability to monitor network flow (e.g. VPC flow logs) to ensure outbound connections are made to only known/trusted hosts.

#### Scalability

- Large enterprises want their chosen DevOps solution to automatically scale based on their number of users and usage patterns.

#### Availability

Large enterprises demand strict uptime SLAs (at least 'three-nines') and expect the following capabilities related to system availability:

- High Availability Deployment (especially their code repositories) including leveraging multiple availability zones within a region to mitigate against an AZ outage.
- Disaster Recovery: Failovers to the included secondary site in case the primary instance experiences an unrecoverable outage and guaranteed recovery times.
- Backups: customers expect their data to be backed up on a regular basis in case of a data loss event and to meet committed recovery point and time objectives during restoration.

#### Performance

- Customers want replicas of their instance to be deployed in multiple regions around the globe to reduce latency for their global user base.

## Where we are Headed
<!--
Describe the future state for your category.
- What problems are we intending to solve?
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is at least minimally realized. In order to challenge your level of ambition
(with the goal to make it sufficiently high), link to the current market leaders long-term vision and address how
we plan to displace them. -->

The main goal for FY23 is to make GitLab Dedicated [available to external customers](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/blob/master/README.md#timeline)(GitLab internal). The initial version of the service will be [fully automated, but will contain a minimal feature set](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/blob/master/README.md#product-development-principle-100-automation-1-features)(GitLab internal). After releasing the offering, we plan to deliver application features and operational capabilities that were [not included in the first launch](https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/#features-that-are-not-available) based on customer demand.

The product will be made available to customers over the course of the following milestones.

### Limited Availability

GitLab Dedicated is available in Limited Availability (LA) today.

The primary goal of this phase is to gain operational experience with a small set of paid, production instances for customers. During this time, improvements to the service will be made to prepare for increased scale, as well as establishing the core foundation required for customers to utilize the service.
Criteria for exiting Limited Availability and entering General Availability:

- Release all features from the [Limited Availability Roadmap](https://about.gitlab.com/direction/saas-platforms/dedicated/#roadmap).
- 100% automated configuration changes, provisioning of new instances, and version updates.
- Reduce the minimum seat count from 2000 to 1000 users (or lower).

### General Availability

General Availability will be driven by the ability of GitLab Dedicated to scale to the expected demand of the service. Customers who want a production-quality GitLab Dedicated instance and are willing to pay for it will be able to get one in this phase, as opposed to LA where instances were limited to selected customers. Targeting expected demand avoids prematurely optimizing for demand significantly beyond what is projected.

In line with GitLab's value of [Iteration](https://about.gitlab.com/handbook/product/product-principles/#iteration), GA will not support all GitLab features at launch, but will provide customers with an enterprise-ready solution that they can integrate into their environments.

The full roadmap for GA [is documented in this Epic](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/485#post-ga-roadmap)(GitLab internal).

### Prioritization Framework

Because Dedicated is a SaaS offering, we prioritize "Keep the lights on" activities (e.g. operating customer environments, tech debt, and corrective actions) above all else to ensure we provide customers a high level of service that continually meets our reliability and performance SLAs. Next, we prioritize security enhancements (e.g. supporting new compliance certifications) since our customer base comprises of large enterprises with strict compliance requirements, followed by initiatives to improve our automation and scaling of the system (including Switchboard), enhancements to our overall system SLAs (uptime, rto/rpo targets, etc), customer feature requests, and then, finally, config changes to customer environments.

When it comes to customer feature requests, we prioritize features that [will benefit the most customers](https://about.gitlab.com/handbook/product/product-processes/#using-the-rice-framework). As a result, we will not build any custom capabilities or one-off features for individual customers. This includes not making modifications to individual tenants. Feature requests that meet our prioritization criteria but are not currently on the roadmap will be slotted in [after other committed roadmap items](#Limited-Availability-Roadmap) have been delivered.

| Priority | Category | Description |
|-----------------------------|--------------------------|-----------------------|
| 0 | KTLO - On Call | Operating Tenant Production Environments |
| 1 | Security/Compliance | Attain security and third party compliance certifications and addressing findings from audits |
| 2 | Automation | Reduce toil required to operate GitLab Dedicated platform including Switchboard (management portal for customer admins) MVC, automated restore, and Automated Version Management  |
| 3 | Reliability | Improve SLAs - Availability, RTO, RPO, Geo, disaster recovery, failover testing |
| 4 | Customer Feature Requests | Delivering new customer-facing platform capabilities as well as enabling GitLab functionality such as Pages, advanced search, reply-by email, service desk |
| 5 | Customer Configuration Changes |  Customers will need config changes during onboarding and weekly maintenance windows. |

### Currently Available Features
Please see list of [Available Features](https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/#available-features) in our documentation.

### Maturity Plan
<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/direction/maturity/).

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->

This category is currently at the `Minimal` maturity level, and our next maturity target is `Viable`, which will be met when the service reaches the General Availability milestone.

### What is Not Planned Right Now
<!-- Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->

Please see list of [Features that are not available](https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/#features-that-are-not-available) in our documentation. In addition to the features from that page, the items from the below list will not be available before the launch of GA but are under consideration for release in the Post-GA launch timeframe.

- Hosted Runners
- Observability Dashboard via Switchboard
- Security and Compliance Phase 3 Items
- Pre-Production Instance
- Custom Domain
- Multiple Geo Secondaries
- Support deploying to additional public cloud providers

## Limited Availability Roadmap

### Q1 and Q2 Summary

The key customer features we plan to deliver in FY24 are summarized below. 

#### Q1
* BYOK
* Quarterly failover tests
* Switchboard internal availability

#### Q2
* Weekly failover tests
* Switchboard for customers
* Private networking improvements

Post Q2, we will prioritize features from the list of [what is not currently planned](#what-is-not-planned-right-now) based on customer demand.

### Phase 1: Tenant Foundations
* _Anticipated Completion Date_: 2022-11-22

#### Environment Automation:
* Planned Team Composition: EM/PM + 5x FTE + 1x Borrow from Geo
* [Completed SOC-2 Audit](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/70) (internal only)
* [Establish Availability targets](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/55)(internal only)
  * [Measure Availability](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/36)(internal only) - Ability to measure availability based on the revised platform definition (Leverage [borrow request](https://gitlab.com/gitlab-com/Product/-/issues/5000) )
* [Automated Version Management - Phase 2](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/29)(internal only): Limited manual effort required to trigger version updates
* Swarm on [FedRAMP needs](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/61)(internal only) to support first milestone.

#### Switchboard:
* Planned Team Composition: 1x FTE + 1x SRE from Environment Automation team 
* Create a release and deployment pipeline for Switchboard in Beta environment
* Add a basic authentication layer

### Phase 2: Switchboard MVC
* _Anticipated Completion Date_: 2022-12-22

#### Environment Automation:
* Planned Team Composition: EM/PM + 6x FTE + 1x Borrow from Geo
* [GitLab Dedicated Private Networking](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/46)(internal only) - provide better control over the network traffic; other initiatives rely on this functionality
* [Geo Phase 1](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/47) (internal only) - Add initial multi-region support
* Continue to swarm on [FedRAMP needs](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/61)(internal only) to support first milestone.
* After `2022-12-01`, Start on [BYOK implementation - new tenants only](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/79)(internal only)

#### Switchboard:
* Planned Team Composition:  1x FTE + 2x Contractors
* [Switchboard Prototype](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/63)
* View and Update Tenant Configuration in the Switchboard UI in Beta environment, connected to the Beta Amp cluster - Config changes for the customer tenant production environments are handled using existing processes.
  - Functionality: UI with a text box accepting a yaml block input; includes validation of user input and communicating validation result
* Initiated initial internal Security review
* Designed SSO integration for GitLab team-members
* Defined SSO integration for tenant access

### Phase 3: Switchboard-based deployments
* _Anticipated Completion Date_: 2023-01-22

#### Environment Automation:
* Planned Team Composition: EM/PM + 7x FTE + 1x Contractor + 1x Borrow from Geo
* [Geo Failover and Recovery](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/48) (internal only)

#### Switchboard:
* Planned Team Composition: EM + 2x FTE + 2x Contractors
* Addressing Internal Security review findings
* Beta Switchboard used by operators, access granted to a customer tenant with running Beta environment
* Switchboard integrated with the production Amp, capable of applying corresponding customer tenant environment
  - Application Security and Compliance approval

### Phase 4: Switchboard for internal Ops
* _Anticipated Completion Date_: 2023-02-22

#### Environment Automation:
* Planned Team Composition: EM/PM + 7x FTE + 1x Contractor
* [GitLab Dedicated Private Networking - Migrations to support a single architecture](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/46)(internal only)
* [Geo Observability](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/50) (internal only)
* [BYOK implementation - new tenants only](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/79)(internal only) completed

#### Switchboard:
* Planned Team Composition: EM/PM + 3x FTE + 2x Contractors
* [Basic non-functional requirements](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/64) - AuthN/AuthZ, tenancy, auditing
* Switchboard used internally by the GitLab Dedicated Team
* Pre-prod and internal customer configurations migrated
* Authentication is done through SSO; Build vs buy decision [documented here](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/195#note_1043249369) (internal only)

### Phase 5: Automated provisioning
* _Anticipated Completion Date_: 2023-03-22

#### Environment Automation:
* Planned Team Composition: EM/PM + 7x FTE + 1x Contractor
* [Automate Restores](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/39) (internal only) - Automated validation of backups, restores to ensure work at scale
* [Continued Security Enhancements - Phase 1 ](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/104)(internal only)
* [Automated Version Management Phase 3](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/58)(internal only): No manual effort required to trigger production tenant version updates. Automated rollouts

#### Switchboard: 
* Planned Team Composition: EM/PM + 3x FTE + 2x Contractors
* AWS account creation automated: AWS tenant accounts automatically created by Switchboard 
* Resolve high-priority security/compliance findings
* Customer onboarding requires no action from GitLab Dedicated team members
  * Internal operators can create a configuration for a new tenant via Switchboard
  * Adding configurations for new tenants and updating configurations for current customers goes through the Switchboard
  * All existing tenant configurations are in Switchboard

### Phase 6: Switchboard UX
* _Anticipated Completion Date_: 2023-04-22

#### Environment Automation:
* Planned Team Composition: EM/PM + 7x FTE + 1x Contractor
* [Geo Phase 4](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/49) (internal only) - Failovers
* [Observability Polish](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/523) (internal only) - Address observability tech debt, centralized monitoring dashboard, provide customers access to their Grafana.
* [Make Dedicated Repos Public](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/33)

#### Switchboard:
* Planned Team Composition: EM/PM + 4x FTE + 2x Contractors
* Customers can self-serve config changes
* Add basic UI controls (drop downs, menu bars, etc) to make it easier for users to update their config rather than pasting a json file into a text box

### Phase 7: Switchboard self-service
* _Anticipated Completion Date_: 2023-05-22

* Assess effectiveness of automation and any required changes, and make sure the team can complete other requirements
  - If additional work is required, avoid increasing future work by adding more tenants that will eventually need to be migrated once any additional work is completed.
 
#### Environment Automation:
* Planned Team Composition: EM/PM + 7x FTE + 1x Contractor
* [Geo Phase 5](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/51) internal only) - Backup
* [Geo Phase 6](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/62) internal only) - Failover verification

#### Switchboard:
* Planned Team Composition: EM/PM + 5x FTE + 2x Contractors
* Ensure reliability and stability of Switchboard
* Existing customers, can use Switchboard to update the configuration for their instances
* Define what is necessary for the integration with customersDot

### Phase 8: Spillover Items
* _Anticipated Completion Date:_ 2023-06-22

* This milestone captures any items moved from previous milestones due to picking up unplanned work (e.g. BYOK)
* [Technical Improvements in LA](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/106) internal only) - Tech debt clean up.

### Phase 9: General availability launch preparations
* _Anticipated Completion Date_: 2023-08-05

#### Environment Automation:
* Planned Team Composition: EM/PM + 9x FTE + 2x Borrows from Geo
* [Continued Security Enhancements - Phase 2 ](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/819)(internal only)
* [Geo Phase 7](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/23) (internal only) - Automated failovers
* Begin focusing on GA items, features
* Production use for broader set of customers
* Available for any customer to onboard

#### Switchboard:
* Planned Team Composition: EM/PM + 6x FTE 
* Integration with customersDot (establish trust relationship, API connectivity)

## Go-to-market plans

### During Limited Availability

_The following links are internal to GitLab_

- [Comms Plan](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/616) 
- [Enablement Plan](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/1479)
- [Field Enablement Guide](https://internal-handbook.gitlab.io/handbook/engineering/horse/field-enablement/index.html) 
- [New Customer Process](https://internal-handbook.gitlab.io/handbook/engineering/horse/#new-customer-process) including Customer Onboarding Plan 
- [Customer Slot Availability and timeline](https://internal-handbook.gitlab.io/handbook/engineering/horse/#customer-slots-leading-up-to-ga) 

### Customer Communication Guidelines

When engaging with customers, we encourage everyone to be very intentional about when and how to mention GitLab Dedicated. This is a new deployment method which will take time to mature, and we do not want to disrupt prospective and existing customers by introducing GitLab Dedicated too early, or when it's a bad fit.

#### Prior to discussing GitLab Dedicated with an opportunity/customer:

1. Review the [target customer profile](#target-customer) to ensure it is a good fit
1. Review the [Limited Availability timeline](https://about.gitlab.com/direction/saas-platforms/dedicated/#limited-availability-roadmap) to ensure it meets the customer's desired onboarding timeframes. Note, there will be a limited number of customers we can support during the Limited Availability period. Once we move to GA, the offering will be available to all customers who are willing to [pay for it](#pricing).
1. Review [pricing](#pricing), as this will cost more than our other offerings and will have a minimum seat count
1. Review the [features unavailable at launch](https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/#features-that-are-not-available) to ensure prospect is comfortable with existing plus planned features.

Please complete the steps below _before_ asking to schedule time with the GitLab Dedicated Product Team.
{: .alert .alert-warning}

##### For prospects interested in adopting the GitLab Dedicated offering:
1. Confirm the customer is eligible for GitLab Dedicated based on the criteria in ["Prior to discussing GitLab Dedicated with an opportunity/customer"](https://about.gitlab.com/direction/saas-platforms/dedicated/#customer-communication-guidelines) section above.
2. Please ask them to join the waitlist, located on the GitLab Dedicated webpage: [https://about.gitlab.com/dedicated/](https://about.gitlab.com/dedicated/)
3. In your SFDC opportunity, set the Intended Product Tier to Dedicated Ultimate OR include Dedicated in the Opportunity name. This is the most important step to ensure we have visibility into the Dedicated pipeline.
4. Once the opportunity is created, post the opportunity to `#f_gitlab_dedicated` in Slack. Product and Sales teams will partner to review and establish potential and timeline.

#### Additional public-facing GitLab Dedicated materials:

We have the following public facing pages available for those interested in learning more:
1. Documentation: [https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/](https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/)
2. Blog post: [https://about.gitlab.com/blog/2022/11/30/introducing-gitlab-dedicated/](/blog/2022/11/30/introducing-gitlab-dedicated/)
3. This Direction page: [https://about.gitlab.com/direction/saas-platforms/dedicated/](direction/saas-platforms/dedicated/)
4. GitLab Dedicated webpage (and waitlist signup): [https://about.gitlab.com/dedicated/](/dedicated/)



### LA Onboarding Timeline

The GitLab Dedicated customer onboarding timeline is available in GitLab's [internal handbook](https://internal-handbook.gitlab.io/handbook/engineering/horse/#LA-Onboarding-Timeline).

### Channel Partner Role in GitLab Dedicated

GitLab Dedicated creates a business opportunity for both GitLab and GitLab channel partners who offer hosted GitLab services. We are creating this offering to meet the needs of customers who want to consume hosted solutions directly from the DevOps vendor (in this case GitLab, Inc.) to more closely align operational and DevOps application needs. Customers who are interested in purchasing hosting services that include value-add managed services (for example, helping meet industry-specific specific requirements like HIPAA) would benefit from working with a Managed Service Provider (MSP) [Partner](https://partners.gitlab.com/English/directory/) Additionally, many customers have existing relationships with Channel Partners. Per the [services ROE](https://about.gitlab.com/handbook/sales/selling-professional-services/), these customers should continue consuming GitLab through their Channel Partner. By following the segmentation laid out above, we can minimize any disruption to existing [channel-sourced ARR](https://gitlab.my.salesforce.com/00O4M000004aqhB)(GitLab internal).

With this effort, we also see an opportunity to grow the overall market for GitLab-hosted services through collaboration with partners. In the short-term we plan to enable MSP partners with [design guidance and certifications](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/1) (GitLab Internal) so that they can offer better hosted GitLab solutions for their customers. We will work with the GitLab Channel Partner team to create a partner certification that will provide MSP partners with technical certifications, training, deployment and maintenance guidelines, and access to some of the same underlying tooling that powers GitLab Dedicated (e.g. GitLab Environment Toolkit and official Reference Architectures). We plan to keep the other underlying services (e.g. Switchboard and Amp) as well as the `GitLab Dedicated` branding proprietary to GitLab, Inc. Longer-term opportunities for consideration include enabling partners [to resell GitLab Dedicated](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/499)(GitLab internal). These partners would provide customers value-added services or integrated vendor stacks while the GitLab Dedicated team would still host the GitLab instance on behalf of the partner.

### Pricing

The pricing for GitLab Dedicated is comprised of the following components:

- Ultimate licenses (2000 seat minimum)
- Infrastructure & Management fee (which covers our costs of operating and hosting the service)
  - Fixed annual fee that varies based on the Infrastructure Size (e.g. S, M, L, XL, XXL)
- Storage
  - Amount of GB the customer wants to buy for their projects. [Free storage quota](https://docs.gitlab.com/ee/user/usage_quotas.html) applies.

#### More details on pricing
During Limited Availability we've priced GitLab Dedicated to align with likely buyers based on minimums and the complexities of their deployments. As mentioned above, the pricing for GitLab Dedicated is comprised of the following components: the [cost for Ultimate Licenses](/pricing/), plus additional fees for instance infrastructure & management, the amount of storage needed, and the customer's infrastructure size (e.g. S, M, L, XL, XXL). As we scale and introduce GitLab Dedicated to more and more customer cohorts we'll share further details on pricing options.

## User success metrics
<!--
- What specific user behaviors are indicate that users are trying these features, and solving their problems?
- How will users discover these features?
-->

- Primary Performance Indicator
  - Paid GMAU
- Secondary Indicators
  - Customer Satisfaction
  - Uplift revenue: defined as revenue the offerings contributes beyond licenses. This includes the infra+management fee plus revenue from consumption pricing.
  - Gross Margin

## Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/product-processes/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

The main competitor to GitLab Dedicated is [GitHub AE](https://docs.github.com/en/github-ae@latest/admin/overview/about-github-ae), which offers a fully-managed GitHub Enterprise instance in the customer's desired region on Azure. Other competitors offering single-tenant, fully-managed devops solutions include AWS CodeCommit, Azure Devops, and GitLab Host.

## Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

This will be filled in during the Limited Availability period once we start receiving feedback from customers.

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

This will be filled in during the Limited Availability period once we start receiving feedback from customers.

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/values/#dogfooding)
the product.-->

This will be filled in during the Limited Availability period once we start receiving feedback from customers.

## Top Strategy Item(s)
<!-- What's the most important thing to move your strategy forward?-->
